# Álbumes escuchados 2020

## Enero

1. Tesseract - Altered State
2. I'm Glad It's You - June
3. Acid Ghost - Warhol
4. Acid Ghost - I Want to Hide My Face and Die
5. Turnover - Peripheral Vision
6. Sadness - I Want to Be There
7. Brand New - The Devil and God Are Raging Inside Me
8. Counterparts - The Difference Between Hell and Home
9. Converge - Jane Doe
10. Radiohead - OK Computer
11. Surf Curse - Buds
12. Surf Curse - Nothing Yet
13. Surf Curse - Heaven Surrounds You

---

## Febrero

1. Tigers Jaw - Tigers Jaw
2. Tigers Jaw -  Charmer
3. Slowdive - Slowdive
4. Radiohead - In Rainbows
5. Radiohead - A Moon Shaped Pool
6. The Microphones - The Glow Pt. 2
7. Radiohead - Amnesiac
8. Grouper - Ruins
9. Grouper - Grid of Points
10. Grouper - Dragging A Dead Deer Up a Hill
11. Minus the Bear - Planet of Ice
12. Agalloch - Ashes Against the Grain
13. Minus the Bear - Menos El Oso
14. Arctic Monkeys - Favourite Worst Nightmare
15. haircuts for men - 逃してしまった関係
16. Grouper - A I A: Alien Observer
17. Harold Budd, Brian Eno - Vol. 2 The Plateaux of Mirror
18. Arctic Monkeys - Whatever People Say I Am, That's What I'm Not
19. Sadness - Leave
20. Neutral Milk Hotel - In the Aeroplane over the Sea
21. Suis La Lune - Distance / Closure
22. Viva Belgrado - Ulises
23. Deafheaven - Sunbather
24. Opeth - Blackwater Park
25. Brian Eno - Ambient 1/Music for Airports
26. Mastodon - Blood Mountain
27. Viva Belgrado - Flores / Carne
28. Giles Corey - Hinterkaifeck
29. Touché Amoré - Stage Four

---

## Marzo

1. Russian Circles - Memorial
2. Mogwai - Mr. Beast
3. Black Hill, heklAa - Rivers & Shores
4. Black Hill, Silent Island -Tales of the Night Forest
5. We Lost the Sea - Departure Songs
6. Shizune - Le voyaguer imprudent
7. The Caretaker -  Everywhere at the End of Time, Stage 1
8. The Caretaker -  Everywhere at the End of Time, Stage 2
9. Shels - Plains of the Purple Buffalo
10. Motorama - Alps
11. Lantlôs - Melting Sun
12. Full of Hell - Trumpeting Ecstasy
13. Zeal & Ardor - The Devil Is Fine
14. Silver Mt. Zion - He Has Left Us Alone But Shafts of Light 15. Sometimes Grace the Corners of Our Rooms
16. Phoenix - Wolfgang Amadeus Phoenix
17. The Strokes - Is This It?
18. mewithoutYou - (Untitled)
19. American Football - American Football
20. Pink Floyd - Animals
21. TTNG - Animals
22. Queens of the Stone Age - ...Like Clockwork
23. Ihsahn - Arktis
24. Whirr - Distressor
25. Nothing - Guilty of Everything
26. Heaven in Her Arms - 白暈 (White Halo)
27. Metallica - Master of Puppets
28. Bedroom - Grow
29. Departures - Death Touches Us from The Moment We Begin to Love
30. Metallica - ...And Justice for All
31. Loma Prieta - Self Portrait
32. Joy Division - Closer
33. Counterparts - You're Not You Anymore

---

## Abril

1. Dikembe - Mediumship
2. Everyone Everywhere - Everyone Everywhere
3. Football, Etc. - The Draft
4. Movements - Feel Something
5. Oathbreaker - Rheia
6. Amenra - Mass VI
7. Cult of Luna - Salvation
8. ISIS - Oceanic
9. Origami Angel - Somewhere City
10. The Saddest Landscape - You Will Not Survive
11. ISIS, Aereogramme - In the Fishtank 14
12. Suis La Lune - Quiet, Pull the strings!
13. ISIS - Panopticon
14. Sauf Les Drones - Lieux Anonymes
15. Alexisonfire - Alexisonfire
16. Marionette ID - Alluvion
17. Holy Fawn - Death Spells
18. Gleemer - Moving Away
19. Scraps of Tape - Scraps of Tape
20. Pity Sex - White Hot Moon
21. Tame Impala - Lonerism
22. Tesseract - Polaris
23. La Dispute - Rooms of the House
24. Pianos Become the Teeth - Keep You
25. Leprous - Pitfalls
26. Leprous - Malina
27. Leprous - Tall Poppy Syndrome
28. The Ocean - Phanerozoic I: Palaeozoic
29. Cloakroom - Infinity

---

## Mayo

1. Tesseract - One
2. Leprous - The Congregation
3. Russian Circles - Guidance
4. Godflesh - Streetcleaner
5. Sadness -  Rain
6. David Bowie - The Rise and Fall of Ziggy Stardust and the Spiders from Mars
7. Audioslave - Audioslave
8. Touché Amore - Is Survived By
9. The Ocean - Heliocentric
10. The Contortionist - Language
11. Converge - All We Love We Leave Behind
12. Panopticon - Autumn Eternal
13. TTNG - This Town Needs Guns
14. An Autumn for Crippled Children - Try Not to Destroy Everything You Love
15. Amon Amarth - Twlight of the Thunder God
16. Amon Amarth - Surtur Rising
17. Alexisonfire - Crisis
18. Respire - Dénouement
19. Cult of Luna, Julie Christmas - Mariner
20. Light Bearer - Silver Tongue
21. Planning for Burial - Below the House
22. Bedroom - Vivid
23. Noah Kittinger - Bloom
24. Tool - Lateralus
25. The World Is a Beautiful Place & I Am No Longer Afraid to Die - Always Foreign
26. Lantlôs - .neon
27. Human Tetris - Memorabilia
28. Lebanon Hannover - Tomb for Two
29. Salvia Plath - melanchole
30. Panopticon - Roads to the North
31. Brand New - Science Fiction

---

## Junio

1. Molchat Doma - Этажи
2. Molchat Doma - С крыш наших домов
3. Zoé - Memo Rex Commander y el Corazón Atómico de la Vía Láctea
4. Dream, Ivory - Dream, Ivory
5. Les discrets - Septembre et ses dernières pensées
6. Thou - Heathen
6. Deafheaven - New Bermuda
7. Whirr - Sway
8. alt-J - An Awesome Wave
9. Bosse-de-Nage - III
10. Thou - Magus
11. Camping in Alask - Please Be Nice
12. CSTVT - Summer Fences
13. Title Fight - Shed
14. Johnny Goth - Far Away
15. Alcest - Kodama
16. Pantera - Vulgar Display of Power
17. Pill Friends - Blessed Suffering
18. The Body - All the Waters of the Earth Turn to Blood
19. Glocca Morra - Just Married
20. Duster - Stratosphere
21. Leporus - Coal
22. CASTLEBEAT - CASTLEBEAT
23. Tiny Moving Parts - Pleasant Living
24. Amesoeurs - Amesoeurs
25. Les Discrets - Prédateurs
26. Sodom - Agent Orange
27. Hermética - Víctimas del Vaciamiento
28. Dads - American Radass
29. Slint - Spiderland
30. Kreator - Outcast
31. No Buses - Boys Loved Her

---

## Julio

1. Red Hot Chili Peppers - By the Way
2. Current Joys - A Different Age
3. José González - In Our Nature
4. Owen - The Avanlanche
5. Mogwai - Happy Songs for Happy People
6. Coldplay - A Rush of Blood to the Head
7. Bright Eyes - I'm Wide Awake, It's Morning
8. Brockhampton - Saturation II
9. Death - Individual Thought Patterns
10. envy - The Fallen Crimson
11. Austere - To Lay Like Old Ashes
12. Jesu - Conqueror
13. Vagrond - Temporal
14. Old Providence - Al Final Solo Hay Vacío
15. Crippled Black Phoenix - I, Vigilante
16. Astronoid - Air
17. Falloch - This Island Our Funeral
18. Jesu - Jesu
19. Bossk - Audio Noir
20. Heretoir - Heretoir
21. Alcest - Les voyages de l'âme
22. Colour - Anthology
23. TTNG Disappointment Island
24. TTNG - 13.0.0.0.0
25. Basement - Colourmeinkindness
26. Gojira - Terra Incognita
27. Breaking Benjamin - Saturate
28. Breaking Benjamin - We Are Not Alone

---

## Agosto

1. Breaking Benjamin - Phobia
2. Breaking Benjamin - Dear Agony
3. Haedes - Haedes
5. Haedes - The Twenty-Second Century
6. Trna - Pattern of Infinity
7. Trna - Earthcult
8. Gojira - The Link
9. Gojira - From Mars to Sirius
10. Gojira - Magma
11. Spiritualized - Ladies and Gentlemen We Are Floating in Space
12. Miles Davis - Ascenseur pour l'échafaud
13. Interpol - Interpol
15. Interpol - Marauder
16. Interpol - Our Love to Admire
17. Interpol - Antic
18. Interpol - El Pintor
19. Interpol - Turn On the Bright Lights
20. Brand New - Deja Entendu
21. Linkin Park - Hybrid Theory
22. Linkin Park - Meteora
23. Ghost - Meliora
24. Gojira - The Way of All Flesh
25. Gojira - L'Enfant Sauvage
26. Radiohead - Hail to the Thief
27. Pentakill - Smite and Ignite
28. Pentakill - II: Grasp of the Undying
29. Radiohead - Kid A
30. alt-J - This Is All Yours
31. Deftones - Koi No Yokan

---

## Septiembre

1. Radiohead- The Bends
2. Radiohead - The King of Limbs
3. Mastodon - Remission
4. Slowdive - Souvlaki
5. Alcest - Souvenirs d'un autre monde
6. Godspeed You! Black Emperor - F#A# Infinity
7. Alcest - Écailles de lune
8. Alcest - Spiritual Instinct
9. Alcest - Shelter
10. Mastodon - Leviathan
11. Mastodon - Crack the Skye
12. We Lost the Sea - The Quietest Place On Earth
13. Black & White (ft. Julian Rodríguez) - Black & White
14. Mastodon - Once More 'Round the Sun
15. Grouper - The Man Who Died in His Boat
16. Grouper - Wide
17. Cult of Luna - Somewhere Along the Highway
18. Cult of Luna - A Dawn to Fear
19. ISIS - Wavering Radiant
20. Sylvaine - Wistful
21. Hanz Zimmer - Interstellar (Original Motion Picture Soundtrack)
22. Cult of Luna - Eternal Kingdom
23. Les Discrets - Ariettes oubliées
24. ISIS - In the Absence of Truth
25. Cult of Luna - Vertikal
26. Departures - Teenage Haze
27. No Vacation - Amo XO
28. Iress - Flaw
29. Svalbard - When I Die, Will I Get Better?
30. Departures - When Losing Everything is Everything You Wanted

---

## Octubre

1. nogfgoodnight - Retrospective (2018)
2. nogfgoodnight - Retrospective (2019)
3. Polaris - The Guilt and the Grief
4. The Helix Nebula - Meridian
5. Ruby Haunt - Tiebreaker
6. Ruby Haunt - Blue Hour
7. Ruby Haunt - The Middle of Nowhere
8. Ikurru - A Picture Frame Full of Memories
9. Ruby Haunt - Hurt
10. Ruby Haunt - Sugar
11. Ed Maverick - mix pa llorar en tu cuarto
12. Emo Side Project - The End of Something
13. The Algorithm - Brute Force
14. Between the Buried and Me - Automata I
15. Between the Buried and Me - Automata II
16. The Smiths - The Smiths
17. Emo Side Project - Ligth Subtracts Itself
18. Muse - Showbiz
19. Soda Stereo - Sueño Stereo
20. Gorillaz - Plastic Beach
21. The Clash - London Calling
22. Gorillaz - Gorillaz
23. Gorillaz - Demon Days
24. The Strokes - First Impressions of Earth
25. The Strokes - Is This It
26. The Strokes - Room On Fire
27. Tigers Jaw - Spin
28. The Smiths - Meat is Murder
29. The Smiths - The Queen Is Dead 

---

## Noviembre

1. The Smiths - Strangeways Here We come
2. Deftones - Saturday Night Wrist
3. Deftones - Gore
4. Giles Corey - Giles Corey
5. Have A Nice Life - The Unnatural World
6. Deftones - Diamond Eyes
7. Deftones - Ohms
8. Deftones - White Pony 
9. Damon Albarn - Everyday Robots
10. Kasabian - Kasabian 
11. Deafheaven - Ordinary Corrupt Human Love
12. Deafheaven - Roads to Judah
13. Muse - Origin of Symmetry
14. Muse - Absolution
15. Muse - Black Holes and Revelations
16. Muse - The Resistance
17. Kasabian - Velociraptor!
18. Kasabian - Empire
19. The Mars Volta - Amputechture
20. Agalloch - Marrow of the Spirit
21. Ulcerate - Stare Into Death and Be Still
22. Panzerfaust - The Suns of Perdition Chapter II: Render Unto Eden
23. Panzerfaust - The Suns of Perdition Chapter I: War, Horrid War
24. Panzerfaust - The Lucifer Principle
25. The Mars Volta - Deloused in the Comatorium
26. Ulcerate - Shrines of Paralysis
27. Death Cab for Cutie - Plans
28. Death Cab for Cutie - Transatlanticism
29. Death - Symbolic
30. Planning for Burial - Leaving

---

## Diciembre

1. Suis La Lune - Riala
2. Ulver - Shadows of the Sun
3. Foxing - Dealer
4. Brockhampton - Ginger
5. Foxing - The Albatross
6. Daft Punk  - Random Access Memories
7. Seahaven - Reverie Lagoon: Music for Escapism Only
8. Thom Yorke - The Eraser
9. Pretend Bones in the Soil, Rust in the Oil
10. Patrick Watson - Love Songs for Robots
11. José González - Veneer
12. José González - Vestiges & Claws
13. Thou - Summit
14. Alexi Murdoch - Time Without Consequence
15. Alexi Murdoch - Towards the Sun
16. Ben Howard - Every Kingdom
17. Her's - Song of Her's
18. Her's - Invitation to Her's
19. envy - Insomniac Doze
20. Gospel - The Moon Is a Dead World
21. Mr Kitty - Time
22. Amenra - Mass III
23. Oathbreaker - Eros Anteros
24. envy - Atheist's Cornea
25. Light Bearer - Lapsus
