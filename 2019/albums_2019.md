# **2019 Albums**

---

## **January**

1. ISIS - Panopticon
2. Giles Corey - Hinterkaifeck
3. Agalloch - The Mantle
4. Chon - Newborn Sun
5. Cult of Luna, Julie Christmas - Mariner
6. Radiohead - OK Computer
7. Slint - Spiderland
8. Have a Nice Life - The Unnatural World
9. Drowse - Cold Air
10. Coldplay - Parachutes
11. Agalloch - Ashes Against the Grain
12. Tigers Jaw - Tigers Jaw
13. Deafheaven - Ordinary Corrupt Human Love
14. Dikembe - Mediumship
15. Arrows in Her - Leaving
16. La Dispute - Wildlife
17. Have a Nice Life - Deathconsciousness
18. Cult of Luna - Salvation
19. ISIS, Aereogramme - In the Fishtank 14
20. The Angelic Process - Weighing Souls With Sand
21. Year of No Light - Ausserwelt
22. Spectral Lore - Gnosis

---

## **February**

1. Drowse - Songs to Sleep On
2. Whirr, Nothing - Whirr & Nothing
3. Whirr - Sway	
4. Tigers Jaw - spin
5. TTNG - Animals
6. TTNG - 13.0.0.0.0
7. Amenra - Mass VI
8. Black & White, Julián Rodríguez - Black & White
9. American Football - American Football
10. Nirvana - Nevermind
11. Godspeed You! Black Emperor - F#A# Infinity
12. MONO - Hymn to the Immortal Wind
13. God Is an Astronaut - All Is Violent, All Is Bright
14. Counterparts - You're Not You Anymore
15. Polar - No Cure No Saviour
16. Idealism - amarantine
17. Tiger Jaw - Charmer
18. Mom Jeans - Best Buds
19. Zoé - Reptilectric
20. +44 - When Your Hearts Stops Beating
21. Charmer - Charmer
22. sports. - Demon Daze
23. Algernon Cadwallader - Fun
24. toe - the book about my idle plot on a vague anxiety
25. toe - songs, ideas, we forgot
26. Quemarlo Todo Por Error - Cuánto Más Hemos Perdido (Yo Ya Perdí la Cuenta)

---

## **March**

1. Minus the Bear - Planet of Ice
2. La Dispute - Somewhere at the Bottom of the River Between Vega and Altair
3. Black Hill & Silent Island - Tales of the night forest
4. Cigarettes After Sex - Cigarettes After Sex
5. Message to Bears - Departures
6. Joy Division - The Best Of
7. Michael Arthur Holloway - Guilt Noir
8. Arctic Monkeys - Whatever People Say I Am, That's What I'm Not
9. Silent Island - Equator
10. Release the Long Ships - Wilderness
11. Arctic Monkeys - Humbug
12. Silent Island - Re-quator
13. Counterparts - The Difference Between Hell and Home
14. Arctic Monkeys - Favourite Worst Nightmare
15. The World Is a Beautiful Place & I Am No Longer Afraid to Die - Formlessness
16. Minus the Bear - Menos el Oso
17. Benedictine Monks of the Abbey of St. Maurice & St. Maur, Clervaux - Gregorian Chants
18. Black Hill - Eclipse
19. Stvannyr - Secrets of the runes
20. Turnover - Peripheral Vision
21. Children of Bodom - Halo of Blood
22. Animal Flag - LP
23. musicformessier - Alien Planet
24. Slow Grave - Dreamless
25. Fugazi - Repeater
26. Interpol - Marauder
27. Departures - Death Touches Us, From the Moment We Begin to Love
28. Pianos Become the Teeth - Keep You
29. Dikembe - Hail Something

---

## **April**

1. Margarita Siempre Viva - La Luz que Dejaste Regar
2. Las Ligas Menores - Las Ligas Menores
3. Alcest - Écailles de Lune
4. Montaña - Coordenadas
5. Marietta - Summer Death
6. .anxious. - Nostalgia: Oldies, Throwaways, and Unfinished Songs from Years Ago
7. .anxious. - Inhale/Exhale/Disappear
8. Boards of Canada - Music Has the Right to Children
9. Killedmyself - Backyard Cementery II
10. ISIS - Wavering Radiant
11. John Coltrane - A Love Supreme
12. Sufjan Stevens - Carrie & Lowell
13. Daft Punk - Random Access Memories
14. Hrvrd - From the Bird's Cage
15. Chillhop Essentials - Spring 2019
16. ISIS - In the Absence of Truth

---

## **May**

1. C418 - Minecraft - Volume Alpha
2. Pink Floyd - Wish You Were Here
3. The Strokes - Room on Fire
4. Billie Eilish - When We All Fall Asleep, Where Do We Go?
5. Mogwai - Mr. Beast
6. ISIS - Oceanic
7. Cult of Luna - Somewhere Along the Highway
8. Death - Symbolic
9. Carcass - Necroticism - Descanting the Insalubrious
10. Taiko - Distinct Ideas
11. Elijah Nang - Gaijin
12. Current Joys - Wild Heart
13. Anatomy of the Bear - Anatomy of the Bear
14. Black Hill - Lost Rivers
15. Chris Punsalan - Since 2008
16. Mastodon - Remission
17. Neurosis - The Eye of Every Storm
18. Hans Zimmer - Blade Runner 2049 Soundtrack
19. Extremoduro - La Ley Innata
20. Death - Human
21. Martin O'Donnell, Michael Salvatori - Halo 2, Vol. 2 (Original Soundtrack)
22. Current Joys - A Different Age
23. José González - Veneer
24. Deftones - Koi No Yokan
25. Panopticon - Roads to the North
26. The Contortionist - Language
27. Falls of Rauros - The Light That Dwells in Rotten Wood
28. Radiohead - A Moon Shaped Pool
29. Current Joys - Me Oh My Mirror
30. A Day to Remember - Homesick
31. C418 - Minecraft - Volume Beta
32. Cult of Luna - Vertikal
33. Radiohead - Amnesiac
34. Behemoth - The Satanist
35. Jinsang - Solitude
36. Althar of Plagues - Teethed Glory & Injury

---

## **June**

1. Moonsorrow - Verisäkeet
2. Interpol - Antics
3. Sepultura - Beneath the Remains
4. Iron Maiden - Powerslave
5. Judas Priest - Painkiller
6. alt-J - An Awesome Wave
7. The World Is a Beautiful Place & I Am No Longer Afraid to Die - Harmlessness
8. Muse - Showbiz
9. Dikembe - Broad Shoulders
10. José González - In Our Nature
11. Soda Stereo - Sueño Stereo
12. Batushka - Litourgiya
13. Drudkh - Forgotten Legends
14. The Caretaker - An Empty Bliss Beyond This World
15. The Caretaker - Everywhere at the End of the Time Stage 1
16. Bohren & Der Club Of Gore - Piano Nights
17. The Caretaker - Patience (After Sebald)
18. Minus the Bear - Acoustics
19. Gojira - L'Enfant Sauvage
20. Kasabian - Kasabian
21. Mastodon - Crack the Skye
22. Alexisonfire - Alexisonfire
23. Luv.Ly - Jazz Crusaders Beattape 2
24. Bohren & Der Club Of Gore - Dolores

---

## **July**

1. Haggard - Eppur Si Muove
2. Septicflesh - Sumerian Daemons
3. Chet Baker, Art Pepper - Picture of Heath
4. Converge - Jane Doe
5. Amon Amarth - Jomsviking
6. Modern Baseball - You're Gonna Miss It All
7. The Promise Ring - 30 Degrees Everywhere
8. Mom Jeans - Puppy Love
9. Chet Baker - The Art of the Ballad
10. Bosse-de-Nage - Further Still
11. Cold Body Radiation - The Orphean Lyre
12. Mogwai - Happy Songs for Happy People
13. Cold Body Radiation - Deer Twilight
14. The Caretaker - Everywhere at the End of the Time (Stage 2)
15. pg.lost - Yes I Am
16. The Caretaker - Everywhere at the End of the Time (Stage 3)
17. Jesu - Conqueror
18. Alcest - Kodama
19. Deafheaven - Sunbather
20. Brand New - Science Fiction
21. Portrayal of Guilt - Let Pain Be Your Guide
22. Deru - 1979
23. Infinite Scale - Living Moments
24. Proem - Before It Finds You
25. Brand New - The Devil and God are Raging Inside Me
26. Radiohead - Hail to the Thief
27. Deafheaven - New Bermuda
28. Team Sleep - Team Sleep
29. The Ocean - Pelagial
30. Touché Amoré - Stage Four
31. Animal Collective - Merriweather Post Pavillion
32. Opeth - Blackwater Park
33. Silver Mt. Zion - He Has Left Us Alone but Shafts of Light Sometimes Grace the Corner of Our Rooms…
34. The Antlers - Hospice
35. mewithoutYou - [Untitled]
36. Cursive - The Ugly Organ
37. Thursday - Full Collapse
38. Minus the Bear - Highly Refined Pirates
39. The Antlers - Burst Apart
40. Proem - Socially Inept
41. Falls of Rauros - Patterns in Mythology
42. alt-J - This Is All Yours
43. Modern Baseball, Marietta - Couples Therapy
44. Bedroom - Grow

---

## **August**

1. Fog Lake - Captain
2. Cemeteries - Barrows
3. CASTLEBEAT - CASTLEBEAT
4. Fox Academy - Elsie
5. Starry Cat -  Starry Cat
6. SALES - Forever & Ever
7. Alex Turner - Submarine, Original Song from the Film
8. Beabadoobee - Patched Up
9. Blonde Tongues - Blonde Tongues
10. Banes World - Drowsy
11. Good Morning - Shawcross
12. No Vacation - Amo XO
13. Blue Hawaii - Untogether
14. Hammock - Maybe They Will Sing for Us Tomorrow
15. Acid Ghost - Warhol
16. Memoryhouse - The Years
17. Tool - Aenima
18. Hammock - Raising Your Voice... Trying to Stop an Echo
19. Viva Belgrado - Ulises
20. Viva Belgrado - Flores, Carne
21. Lisel - Angels on the Slope
22. State Faults - Clairvoyants
23. Meniscus - war of currents
24. La Dispute - Panorama
25. Meniscus - Refractions
26. Noah Kittinger - Bloom
27. Bedroom - Vivid
28. Foxing - The Albatross
29. It Looks Sad - Self-Titled
30. It Looks Sad - Sky Lake
31. I'm Glad Its You - June
32. Él Mató a Un Policía Motorizado - La Dinastía Scorpio
33. I'm Glad Its You - The Things I Never Say
34. The Hotelier - Home, Like No Place Is There
35. Tiny Moving Parts - Pleasant Living
36. Cold Body Radiation - A Clear Path
37. Alcest - Shelter
38. Sólstafir - Ótta
39. Hammock - Chasing After Shadows... Living With the Ghosts
40. An Autumn for Crippled Children - Try Not to Destroy Everything You Love
41. Old Silver Key - Tales of Wanderings
42. Ulver - Bergtatt
43. Burzum - Filosofem
44. The Gregorian Chants Ensemble - Gregorian Chants
45. Benedictine Monks of Silos - Gregorian Chant: The Definitive Collection
46. Giles Corey - Giles Corey
47. Coro de la Abadía Benedictina de Santo Domingo de Silos - Lumières du grégorien

---

## **September**

1. Acid Ghost - I Want to Hide My Face & Die
2. Tool - Fear Inoculum
3. Interpol - Turn on the Bright Lights
4. Gojira - The Way of All Flesh
5. Interpol - A Fine Mess
6. Bohren & Der Club Of Gore - Midnight Radio
7. Franz Ferdinand - Tonight: Franz Ferdinand
8. Vetusta Morla - Un Día en el Mundo
9. Don't You(,) Mean People? - Who Cares?
10. Planning for Burial - Quietly
11. Gojira - The Link
12. Jesu -  Jesu
13. Gojira - Magma
14. Movements - Feel Something
15. Mgła - Exercises in Futility
16. Hundredth - RARE
17. Movements - Outgrown Things
18. Uneven Structure - Februus
19. Brand New - Deja Entendu
20. Manchester Orchestra - A Black Mile to the Surface
21. Gnarls Barkley - St. Elsewhere
22. Nile - Those Whom the Gods Detest
23. Joy Division - Unknown Pleasures
24. Emo Side Project - The End of Something
25. JPEGMAFIA - Veteran
26. September Malevolence - After This Darkness, There's a Next
27. Suis La Lune - Heir
28. La Dispute - Rooms of the House
29. José González - Vestiges & Claws
30. Alexi Murdoch - Time Without Consequence
31. Deftones - Deftones
32. Emo Side Project - Light Subtracts Itself
33. Radiohead - Kid A

---

## **October**

1. ef - Mourning golden Morning
2. The Algorithm - Brute Force
3. Mohican - The Abuse Is Character
4. Old Providence - Al Final Solo Hay Vacío
5. Botanist - VI: Flora
6. Carissa's Weird - They'll Only Miss You When You Leave: Songs 1996-2003
7. Planning For Burial - Below the House
8. Deftones - Diamond Eyes
9. Tesseract - Polaris
10. Mogwai - Rock Action
11. Grouper - Ruins
12. Miles Davis -  Ascenseur pour l'échefaud
13. Football, Etc - The Draft
14. Pete Davis - False Friends EP
15. Maybeshewill - Not For Want of Trying
16. Maybeshewill - I Was Here for a Moment, Then I Was Gone
17. Duster - Stratosphere
18. Grouper - Grid of Points
19. The Microphones - The Glow, Pt. 2
20. One Day of December - Beginning of the End
21. How to Disappear Completely - Mer de Revs
22. Mineral - The Power of Failing
23. Lantlôs - Melting Sun
24. How to Disappear Completely - Mer de Revs II
25. Brian Eno - Ambient 1: Music for Airports
26. Bing & Ruth - No Home of the Mind
27. Inside the Whale - Manifest
28. William Basinski, Lawrence English - Selva Oscura
29. How to Disappear Completely - Mer de Revs III
30. Cult of Luna - A Dawn to Fear
31. Leprous - Pitfalls
32. Alcest - Spiritual Instinct
33. Chalk Hands - Burrows & Other Hideouts
34. Locrian - Return to Annihilation
35. Pianos Become the Teeth - Wait for Love
36. Bad Religion - Suffer

---

## **November**

1. Orchid - Dance Tonight! Revolution Tomorrow!
2. Counterparts - Nothing Left to Love
3. Coldworld - Melancholie
4. Counterparts - Tragedy Will Find Us
5. The Spirit of the Beehive - You Are Arrived (But You've Been Cheated)
6. Agalloch - Marrow of the Spirit
7. Nachtreich, Spectral Lore - The Quivering Lights
8. Nachtreich - Sturmgang
9. Choir of St. Abuscus - Gregorian Chants, Vol. 2
10. Coffin Problem - Coffin Problem
11. The Hold Steady - Separation Sunday
12. Chet Baker - She Was Too Good To Me
13. Interpol - El Pintor
14. Dystopian Future Movies - Time
15. Grouper - Dragging a Dead Deer Up a Hill
16. Destroyalldreamers - À Coeur Léger Sommeil Sanglant
17. Vaura - The Missing
18. Interpol - Our Love to Admire
19. Interpol - Interpol
20. Franz Ferdinand - Franz Ferdinand
21. Industries of the Blind - Chapter 1: Had We Known Better
22. 1969 - Maya
23. Dotan - 7 Layers
24. Faunts - Feel.Love.Thinking.Of.
25. Sun Kil Moon - Benji
26. Nicola Cruz - Prender el Alma
27. Farewell Poetry -  Hoping for the Invisible to Ignite

---

## **December**

1. The World Is a Beautiful Place & I Am No Longer Afraid to Die - Whenever, If Ever
2. Glocca Morra - Just Married
3. TTNG - Disappointment Island
4. TTNG - This Town Needs Guns
5. Nachtreich - B-Sides & Unreleased Demos
6. Colour - Anthology
7. Arcade Fire - The Suburbs
8. How to Disappear Completely - Seraphim
9. Amenra - Mass V
10. Jesu - Every Day I Get Closer to the Light from Which I Came
11. Grails - Chalice Hymnal
12. Amenra - Mass III
13. Neurosis - Given to the Rising
14. Motorama - Alps
15. Whirr - Distressor
16. Death Cab for Cutie - Plans
17. Nothing - Guilty of Everything
18. Love of Lesbian - El Poeta Halley
19. Mount Eerie - A Crow Looked at Me
20. Have a Nice Life - Sea of Worry
21. Origami Angel - Somewhere City
22. Deftones - White Pony
23. The World Is a Beautiful Place & I Am No Longer Afraid to Die - Always Foreign
24. empite empire (i was a lonely state) - What It Takes to Move Forward
25. Zoé - Memo Rex Commander y el Corazón Atómico de la Vía Láctea
26. Free Throw - Those Days Are Gone
27. alt-J - Relaxer
28. Touché Amoré - Is Survived By
29. Touché Amoré - ...To the Beat of a Dead Horse
30. Woods of Ypres - Woods 5: Grey Skies & Electric Light
31. Panopticon - Autumn Eternal
32. Falls of Rauros - Vigilance Perennial

---
Total (372)
